var map;
var infoWindow;
var triangulo;
var recorrido360=[];
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 18,//17.079355, -96.745080
    center: {lat:  17.079355, lng: -96.745080},
    mapTypeId: 'terrain'
  });
    downloadUrl('http://localhost/practicaemergentes/PHP/Dompoli.php', function(data) {
      var xml = data.responseXML;
      var markers = xml.documentElement.getElementsByTagName('marker');
      Array.prototype.forEach.call(markers, function(markerElem) {
  	     var id = markerElem.getAttribute('nombre');
         var lat = markerElem.getAttribute('latitudes');
  	     var log = markerElem.getAttribute('longitudes');
         var des = markerElem.getAttribute('descripcion');
         var imgp = markerElem.getAttribute('img');
         var img360=markerElem.getAttribute('img360');
         recorrido360.push([id,img360]);
  	     var lats = lat.split(",");
  	     var logs = log.split(",");
  	     var triangleCoords = [];
  	     for (var indice in lats) {
           triangleCoords.push({lat:parseFloat(lats[indice]),lng:parseFloat(logs[indice])});
         }
         var bermudaTriangle;
  	     // Construct the polygon.
         switch (markerElem.getAttribute('tipo')){
           case 'aulas':
            bermudaTriangle = new google.maps.Polygon({
             paths: triangleCoords,
             strokeColor: '#FF0000',
             strokeOpacity: 0.45,
             strokeWeight: 2,
             fillColor: '#FF0000',
             fillOpacity: 0.4
            });
            break;
            case 'administrativos':
             bermudaTriangle = new google.maps.Polygon({
              paths: triangleCoords,
              strokeColor: '#0489B1',
              strokeOpacity: 0.1,
              strokeWeight: 2,
              fillColor: '#0489B1',
              fillOpacity: 0.4
             });
             break;
             case 'laboratorios':
              bermudaTriangle = new google.maps.Polygon({
               paths: triangleCoords,
               strokeColor: '#0000FF',
               strokeOpacity: 0.4,
               strokeWeight: 1,
               fillColor: '#0000FF',
               fillOpacity: 0.4
              });
              break;
              case 'deportivo':
               bermudaTriangle = new google.maps.Polygon({
                paths: triangleCoords,
                strokeColor: '#00FF40',
                strokeOpacity: 0.45,
                strokeWeight: 2,
                fillColor: '#00FF40',
                fillOpacity: 0.4
               });
               break;
         }
         bermudaTriangle.setMap(map);
		     bermudaTriangle.addListener('mouseover',function(event){
           var contentString = "<div><label class='titulo' name='nombre' id='nombre'>" + id+ "</label><br>Mas detalles dar click " ;
           emergente.setContent(contentString);
           emergente.setPosition(event.latLng);
           emergente.open(map);
		     });
		     bermudaTriangle.addListener('mouseout',function(event){
			        emergente.close();
		     });
  		   infowindow = new google.maps.InfoWindow;
		     emergente = new google.maps.InfoWindow;
  		   bermudaTriangle.addListener('click', function(event) {
			     emergente.close();
           var contentString = "<div><label class='titulo' name='nombre' id='nombre'>" + id+ "</label><br>Corrdenas: " + event.latLng.toUrlValue(6);
           contentString += "<br><span class='texto'>Descripcion: "+des+"</span>";
           if(imgp!="")
             contentString+="<div style='text-align: center;'><img src='"+imgp+"' style='width:200px;height:200px;'></div>";
           if(img360!="")
             contentString+="<button class='boton' onclick='recorridoImg()'>Recorrido 360</button></div>";
           infowindow.setContent(contentString);
           infowindow.setPosition(event.latLng);
           infowindow.open(map);
        });
  	  });
    });
}
function recorridoImg(){
  var nom=document.getElementById('nombre').innerHTML;
  var cord;
  for(var i=0;i<recorrido360.length;i++){
    if(recorrido360[i][0]==nom){
      cord=recorrido360[i][1];break;
    }
  }
  document.getElementById('reco').innerHTML="<div class='recorrido'><iframe width='100%' height='560px' src='"+cord+"' frameborder='0' style='border:none;' allowvr='yes' allow='vr; xr; accelerometer; magnetometer; gyroscope; autoplay;' allowfullscreen mozallowfullscreen='true' webkitallowfullscreen='true' onmousewheel='' ></iframe><button onclick='quitar()'>Salir del Recorrido</button></div>";
}
function quitar(){
  var div = document.getElementById('reco');
   div.removeChild(div.lastChild);
}
function downloadUrl(url, callback) {
  var request = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest;
  request.onreadystatechange = function() {
    if (request.readyState == 4) {
      request.onreadystatechange = doNothing;
      callback(request, request.status);
    }
  };
  request.open('GET', url, true);
  request.send(null);
}
function doNothing() {}
