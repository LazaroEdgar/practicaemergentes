-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-10-2018 a las 08:59:09
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mapaito`
--
CREATE DATABASE IF NOT EXISTS `mapaito` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `mapaito`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrativos`
--

CREATE TABLE `administrativos` (
  `id` int(8) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `latitudes` text COLLATE utf8_spanish_ci NOT NULL,
  `longitudes` text COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `imagenPlana` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `imagen3d` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `administrativos`
--

INSERT INTO `administrativos` (`id`, `nombre`, `latitudes`, `longitudes`, `descripcion`, `imagenPlana`, `imagen3d`) VALUES
(1, 'Direccion y Subdireccion', '17.077548,17.077451,17.077268,17.077364,17.077548\r\n', '-96.745249,-96.744958,-96.745025,-96.745317,-96.745249\r\n', 'PLANTA ALTA: Direccion, Subdirecciones Academica, Planeacion y Vinculacion y Administrativa; Depto. Rec. Financieros. PLANTA BAJA: Depto. Planeacion Prog. y Presup.; Depto. de Comunicacion y Dif.; Depto. de Gestion Tec. y Vinculacion y Depto. de Rec. Humanos.', '', ''),
(2, 'Servicios Escolares', '17.077794,17.077695,17.077519,17.077615,17.077794', '-96.745162,-96.744868,-96.744932,-96.745228,-96.745162', 'Depto. de Estudios Profesionales y Depto. Servicios Escolares.', 'recursos/administrativos/div_est_prof.jpg', ''),
(3, 'Desarrollo Academico', '17.078058,17.077950,17.077837,17.077945,17.078058', '-96.745675,-96.745358,-96.745400,-96.745718,-96.745675', 'Depto. de Desarrollo Academico; Aula Magna, Sala de Usos Multiples, SUM.', 'recursos/administrativos/des_academico.jpg', ''),
(4, 'Audiovisual de Ingenieria', '17.078360,17.078262,17.078141,17.078238,17.078360', '-96.745974,-96.745667,-96.745709,-96.746017,-96.745974', 'Audiovisual de Ingenieria, Conmutador y Sanitarios.', 'recursos/administrativos/audio_ing.jpg', ''),
(5, 'Sala de Titulacion', '17.078426,17.078392,17.078041,17.078077,17.078426', '-96.745254,-96.745132,-96.745252,-96.745379,-96.745254', 'Sala de Titulacion, Aula de Dibujo y Sanitarios.', 'recursos/administrativos/sala_titulacion.jpg', 'https://www.google.com/maps/embed?pb=!4v1540943327301!6m8!1m7!1sCAoSLEFGMVFpcE8yZXFzaFNaZmRVQUdNYWZtRUFhY2lmYVF4N3lyMnBJaVh2c3po!2m2!1d17.0781661996037!2d-96.7451720301691!3f272.9470985340069!4f-1.3526199830635903!5f0.7820865974627469'),
(6, 'Maestria en Construccion', '17.078870,17.078955,17.078911,17.078828,17.078870', '-96.745917,-96.745738,-96.745712,-96.745881,-96.745917', 'Maestria en Construccion.', 'recursos/administrativos/maestria_construccion.jpg', ''),
(7, 'Depto. de Ing. Electrica', '17.078910,17.078831,17.078611,17.078683,17.078910', '-96.745456,-96.745199,-96.745273,-96.745525,-96.745456', 'Depto. de Ing. Electrica, Depto. de Serv. Grales., Programa de Tutorias.', 'recursos/administrativos/dep_electric.jpg', ''),
(8, 'Centro de Informacion', '17.078900,17.078834,17.078636,17.078657,17.078600,17.078579,17.078444,17.078511,17.078646,17.078625,17.078681,17.078705,17.078900', '-96.745114,-96.744903,-96.744973,-96.745035,-96.745055,-96.744991,-96.745037,-96.745245,-96.745197,-96.745131,-96.745112,-96.745184,-96.745114', 'Edificio de Educacion a Distancia, Centro de Informacion, Aula Magna.', 'recursos/administrativos/centro_informacion.jpg', 'https://poly.google.com/view/cDLn4K65IxB/embed?chrome=min'),
(9, 'Recursos Materiales', '17.079230,17.079191,17.078943,17.078977,17.079230', '-96.744287,-96.744167,-96.744253,-96.744367,-96.744287', 'Depto. de Recursos Materiales.', 'recursos/administrativos/rec_materiales.jpg', ''),
(10, 'Quimica e Ingenieria Industrial', '17.079571,17.079475,17.079379,17.079458,17.079571', '-96.744006,-96.743726,-96.743762,-96.744045,-96.744006', 'Depto. de Quimica, Depto. de Ingenieria Industrial.', 'recursos/administrativos/dep_quim_industrial.jpg', ''),
(11, 'Cubiculos de Posgrado', '17.079191,17.079085,17.078967,17.079080,17.079191', '-96.744010,-96.743663,-96.743705,-96.744060,-96.744010', 'Cubiculos de Posgrado.', 'recursos/administrativos/cubiculos_posgrado.jpg', ''),
(12, 'Posgrado', '17.079080,17.079003,17.078856,17.078932,17.079080', '-96.743612,-96.743373,-96.743425,-96.743664,-96.743612', 'Posgrado (Claustro Doctoral).', 'recursos/administrativos/posgrado.jpg', 'https://www.google.com/maps/embed?pb=!4v1540941200549!6m8!1m7!1sCAoSLEFGMVFpcE9qT09fNXVHWEs5amE3T1NnNndDTnFiU1hhckFuMklfNUdab21U!2m2!1d17.07903058553858!2d-96.74362470638998!3f207.55805705685856!4f14.62180759769791!5f0.7820865974627469'),
(13, 'Ciencias de la Tierra', '17.078514,17.078467,17.078350,17.078398,17.078514', '-96.744252,-96.744090,-96.744129,-96.744290,-96.744252', 'Depto. de Ciencias de la Tierra.', 'recursos/administrativos/dep_ciencias_tierra.jpg', 'https://www.google.com/maps/embed?pb=!4v1540942046870!6m8!1m7!1sCAoSLEFGMVFpcFA2UHRuNld3bGozSExVTTdTWWhpV0lTTDhjTjYzS2IzRzlYeDhz!2m2!1d17.07842387220314!2d-96.74431530510164!3f114.9845055560424!4f-15.839802699523162!5f0.7820865974627469'),
(14, 'Sala de Maestros', '17.077795,17.077747,17.077631,17.077674,17.077795', '-96.744615,-96.744457,-96.744496,-96.744657,-96.744615', 'Sala de Maestros.', 'recursos/administrativos/sala_maestros.jpg', ''),
(15, 'Edificio en Construccion', '17.077968,17.077877,17.077488,17.077578,17.077968', '-96.744267,-96.743969,-96.744100,-96.744398,-96.744267', 'Edificio en Construccion.', '', ''),
(16, 'Gimnasio', '17.077911,17.077822,17.077428,17.077518,17.077911', '-96.743753,-96.743447,-96.743573,-96.743879,-96.743753', 'Gimnasio.', 'recursos/administrativos/gimnasio.jpg', ''),
(17, 'Cafeteria', '17.077413,17.077357,17.077154,17.077206,17.077413', '-96.744071,-96.743889,-96.743959,-96.744141,-96.744071', 'Cafeteria.', 'recursos/administrativos/cafeteria.jpg', ''),
(18, 'Almacen', '17.076869,17.076831,17.076606,17.076646,17.076869', '-96.743375,-96.743250,-96.743330,-96.743451,-96.743375', 'Almacen.', 'recursos/administrativos/almacen.jpg', ''),
(19, 'Maestria en Docencia', '17.076621,17.076581,17.076237,17.076266,17.076621', '-96.743749,-96.743636,-96.743758,-96.743872,-96.743749', 'Maestria en Docencia, Cubiculos y Sanitarios.', 'recursos/administrativos/maestria_docencia.jpg', ''),
(20, 'Maestria en Administracion', '17.076408,17.076366,17.076296,17.076333,17.076408', '-96.744350,-96.744240,-96.744267,-96.744377,-96.744350', 'Maestria en Administracion.', 'recursos/administrativos/maestria_administracion.jpg', ''),
(21, 'Sala de Titulacion de Maestria', '17.076238,17.076183,17.076096,17.076149,17.076238', '-96.744239,-96.744090,-96.744117,-96.744271,-96.744239', 'Sala de Titulacion de Maestria.', 'recursos/administrativos/sala_titulacion_maestria.jpg', ''),
(22, 'Ciencias Economico Administrativas', '17.076373,17.076264,17.076154,17.076254,17.076373', '-96.744631,-96.744289,-96.744329,-96.744662,-96.744631', 'Depto. de Ciencias Economico Administrativas.', 'recursos/administrativos/dep_ciencias_economico_a.jpg', ''),
(23, 'Audiovisual de Licenciatura', '17.076603,17.076512,17.076405,17.076494,17.076603', '-96.744776,-96.744477,-96.744515,-96.744803,-96.744776', 'Delegacion Sindical y Audiovisual de Licenciatura.', 'recursos/administrativos/audio_lic.jpg', ''),
(24, 'Servicio Medico', '17.076823,17.076793,17.076709,17.076651,17.076737,17.076701,17.076594,17.076721,17.076823', '-96.744791,-96.744684,-96.744706,-96.744542,-96.744523,-96.744415,-96.744461,-96.744829,-96.744791', 'Cegrito, Grinf., Servicio Medico, Sanitarios y Aulas (H1 y H2).', 'recursos/administrativos/enfermeria.jpg', ''),
(25, 'Ciencias Basicas', '17.077016,17.076921,17.076827,17.076922,17.077016', '-96.744660,-96.744376,-96.744410,-96.744695,-96.744660', 'Depto. de Ciencias basicas, Salas de Maestros A y B.', 'recursos/administrativos/dep_ciencias_basicas.jpg', ''),
(26, 'Sistemas y Computacion', '17.076295,17.076182,17.076146,17.076256,17.076295', '-96.744762,-96.744701,-96.744781,-96.744846,-96.744762', 'Depto. de Sistemas y Computacion.', 'recursos/administrativos/dep_sistemas.jpg', ''),
(27, 'Coordinacion de Sistema Abierto', '17.076146,17.076256,17.076197,17.076086,17.076146', '-96.744781,-96.744846,-96.744947,-96.744879,-96.744781', 'Coordinacion de Sistema Abierto.', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aulas`
--

CREATE TABLE `aulas` (
  `id` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `latitudes` text COLLATE utf8_spanish_ci NOT NULL,
  `longitudes` text COLLATE utf8_spanish_ci NOT NULL,
  `aulas` int(4) NOT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci NOT NULL,
  `imagenPlana` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `imagen3d` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `aulas`
--

INSERT INTO `aulas` (`id`, `nombre`, `latitudes`, `longitudes`, `aulas`, `descripcion`, `imagenPlana`, `imagen3d`) VALUES
('A', 'Edificio A', '17.0784797,17.0783665,17.0782692,17.0783825,17.0784797', '-96.7457621,-96.7457980,-96.7454625,-96.7454265,-96.7457621', 8, 'Edificio A, aulas: A1,A2,A3,A4,A5,A6,A7,A8.', 'recursos/edificios/edificio_a.jpg', 'https://www.google.com/maps/embed?pb=!4v1540943788291!6m8!1m7!1sCAoSLEFGMVFpcE1Nb25aSGMtXzlzOHQ4Yzd0S0lwM2VJbDY0bzY1QUFIdEtZS0F6!2m2!1d17.07830426675548!2d-96.74566312087663!3f61.32675400183171!4f-8.403644263582521!5f0.7820865974627469'),
('B', 'Edificio B', '17.078413,17.078324,17.078190,17.078277,17.078413', '-96.744975,-96.745011,-96.744642,-96.744607,-96.744975', 9, 'Edificio B, aulas: B1,B2,B3,B4,B5,B6,B7,B8,B9 ', 'recursos/edificios/edificio_b.jpg', 'https://poly.google.com/view/4sxKER3V-Le/embed?chrome=min'),
('C', 'Edificio C', '17.078127,17.078036,17.077943,17.078033,17.078127', '-96.744970,-96.745004,-96.744726,-96.744692,-96.744970', 9, 'Edificio C, aulas: C1,C2,C3,C4,C5,C6,C7,C8,C9 ', 'recursos/edificios/edificio_c.jpg', 'https://poly.google.com/view/1TUrTiMmdTW/embed?chrome=min'),
('D', 'Edificio D', '17.077290,17.077173,17.077093,17.077209,17.077290', '-96.743714,-96.743757,-96.743518,-96.743476,-96.743714', 4, 'Edificio D, aulas: D1,D2,D3,D4', 'recursos/edificios/edificio_d.jpg', ''),
('E', 'Edificio E', '17.077094,17.076974,17.076865,17.076987,17.077094', '-96.744265,-96.744306,-96.743967,-96.743924,-96.744265', 3, 'Edificio E, aulas: E1,E2,E3', 'recursos/edificios/edificio_e.jpg', ''),
('F', 'Edificio F', '17.076818,17.076679,17.076573,17.076714,17.076818', '-96.744261,-96.744306,-96.743961,-96.743916,-96.744261', 8, 'Edificio F, aulas: F1,F2,F3,F4,F5,F6,F7,F8', 'recursos/edificios/edificio_f.jpg', ''),
('G', 'Edificio G', '17.076552,17.076436,17.076340,17.076458,17.076552', '-96.744287,-96.744328,-96.744044,-96.744002,-96.744287', 4, 'Edificio G, aulas: G1,G2,G3,G4', 'recursos/edificios/edificio_g.jpg', ''),
('H', 'Edificio H', '17.076811,17.076699,17.076580,17.076690,17.076811', '-96.744801,-96.744839,-96.744433,-96.744392,-96.744801', 2, 'Edificio H, aulas: H1,H2', 'recursos/edificios/edificio_h.jpg', ''),
('I', 'Edificio I', '17.076282,17.076188,17.075844,17.075940,17.076282', '-96.744755,-96.744947,-96.744745,-96.744551,-96.744755', 14, 'Edificio I, aulas: I1,I2,I3,I4,I5,I6,I7,I8,I9,I10,I11,I12,I13,I14', 'recursos/edificios/edificio_i.jpg', 'https://poly.google.com/view/468lv6CHZDJ/embed?chrome=min'),
('J', 'Edificio J', '17.076347,17.076240,17.076143,17.076252,17.076347', '-96.744639,-96.744679,-96.744372,-96.744334,-96.744639', 1, 'Tipo', 'recursos/edificios/edificio_j.jpg', ''),
('K', 'Edificio K', '17.076656,17.076318,17.076281,17.076547,17.076559,17.076631,17.076656', '-96.743728,-96.743852,-96.743742,-96.743644,-96.743678,-96.743653,-96.743728', 1, 'Edificio K, aulas: K1', 'recursos/edificios/edificio_k.jpg', ''),
('L', 'Edificio L', '17.078178,17.078108,17.078086,17.078158,17.078178', '-96.744199,-96.744207,-96.744003,-96.743996,-96.744199', 2, 'Edificio L, aulas: L1,L2', 'recursos/edificios/edificio_l.jpg', ''),
('M', 'Edificio M', '17.078611,17.078547,17.078522,17.078592,17.078611', '-96.744136,-96.744158,-96.744080,-96.744055,-96.744136', 1, 'Edificio M, aulas: M1', 'recursos/edificios/edificio_m.jpg', ''),
('N', 'Edificio N', '17.078758,17.078692,17.078673,17.078738,17.078758', '-96.744066,-96.744086,-96.744028,-96.744005,-96.744066', 1, 'Edificio N, aulas: N1', 'recursos/edificios/edificio_n.jpg', ''),
('N.', 'Edificio N.', '17.079342,17.079272,17.079252,17.079320,17.079342', '-96.743710,-96.743732,-96.743658,-96.743636,-96.743710', 1, 'Edificio N., aulas: N.1', 'recursos/edificios/edificio_ñ.jpg', ''),
('O', 'Edificio O', '17.078157,17.078051,17.078033,17.078137,17.078157', '-96.743345,-96.743375,-96.743296,-96.743266,-96.743345', 2, 'Edificio O, aulas: O1,O2', 'recursos/edificios/edificio_o.jpg', ''),
('P', 'Edificio P', '17.078035,17.077939,17.077894,17.077991,17.078035', '-96.743386,-96.743418,-96.743273,-96.743242,-96.743386', 2, 'Edificio P, aulas: P1,P2', 'recursos/edificios/edificio_p.jpg', ''),
('S', 'Edificio S', '17.076455,17.076238,17.076216,17.076431,17.076455', '-96.743655,-96.743727,-96.743654,-96.743576,-96.743655', 4, 'Edificio S, aulas: S1,S2,S3,S4', 'recursos/edificios/edificio_s.jpg', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deportivo`
--

CREATE TABLE `deportivo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `latitudes` text COLLATE utf8_spanish_ci NOT NULL,
  `longitudes` text COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `imagenPlana` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `imagen3d` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `deportivo`
--

INSERT INTO `deportivo` (`id`, `nombre`, `latitudes`, `longitudes`, `descripcion`, `imagenPlana`, `imagen3d`) VALUES
(1, 'Estadio Tecnologico de Futbol', '17.079656,17.081196,17.081216,17.079753,17.079656', '-96.747048,-96.747207,-96.745952,-96.745862,-96.747048', 'Estadio de Futbol del Instituto Tecnologico de Oaxaca. Es usado por el equipo Alebrijes de Oaxaca. Capacidad: 17200 espectadores.', '', ''),
(2, 'Microestadio y pista de atleti', '17.079916,17.079779,17.080040,17.081073,17.081443,17.081069,17.079916', '-96.745064,-96.744648,-96.744249,-96.744251,-96.744727,-96.745230,-96.745064', 'Microestadio de futbol, al rededor de el se encuentra la pista de atletismo', '', 'https://www.google.com/maps/embed?pb=!4v1540940548610!6m8!1m7!1sCAoSLEFGMVFpcE1fMGFWRkJIYTJqeV90R19lQm9fMU1CeERlbnVvb1hQQ0FvRHVh!2m2!1d17.08076536655092!2d-96.74480753269245!3f0!4f-4.161000000000001!5f0.7820865974627469'),
(3, 'Diamante de Baseball', '17.081047,17.081913,17.082477,17.081843,17.081047', '-96.745423,-96.744679,-96.745474,-96.746154,-96.745423', 'Diamante de Baseball', '', 'https://www.google.com/maps/embed?pb=!4v1540940695209!6m8!1m7!1sCAoSLEFGMVFpcFBzMm82OVNHTGJ4UC12c0F1Ylc5Z0RSVXJIT0Uwc1JXWkxnZ25m!2m2!1d17.08128169869607!2d-96.74512743771682!3f322.45979061034586!4f-6.983211223637426!5f0.7820865974627469'),
(4, 'Alberca de Instituto Tecnologi', '17.081840,17.081839,17.081349,17.081318,17.081840', '-96.746485,-96.746250,-96.746205,-96.746439,-96.746485', 'Alberca de Instituto Tecnologico de Oaxaca', '', 'https://www.google.com/maps/embed?pb=!4v1540939686944!6m8!1m7!1sCAoSLEFGMVFpcE5tTnktQkhrSGV3cTlPWEdlaHZGemtEU3BZTzdzODFNU2Q4MXJ0!2m2!1d17.08158478242197!2d-96.74650197744195!3f156.01451059804424!4f0.04253734673238796!5f0.7820865974627469'),
(5, 'Cancha de Futbol 7', '17.081829,17.081370,17.081368,17.081831,17.081829', '-96.746770,-96.746738,-96.746541,-96.746565,-96.746770', 'Cancha de Futbol 7', '', 'https://www.google.com/maps/embed?pb=!4v1540939498343!6m8!1m7!1sCAoSLEFGMVFpcE5tTnktQkhrSGV3cTlPWEdlaHZGemtEU3BZTzdzODFNU2Q4MXJ0!2m2!1d17.08158478242197!2d-96.74650197744195!3f294.40247!4f-5.834599999999995!5f0.7820865974627469'),
(6, 'Cancha mixta de futbol y basqu', '17.081757,17.081757,17.081593,17.081295,17.081306,17.081460,17.081757', '-96.746779,-96.746949,-96.747344,-96.747321,-96.747119,-96.746768,-96.746779', 'Canchas mixtas de futbol y basquetbol', '', 'https://www.google.com/maps/embed?pb=!4v1540940883686!6m8!1m7!1sCAoSLEFGMVFpcE82QUtRQkEwWm4wS3NHTmZSWUVvZVEtcVVGWnZzRG9lNUk4Nl9v!2m2!1d17.08146818900427!2d-96.74718362277366!3f191.2322248902735!4f-2.4992918842087164!5f0.7820865974627469'),
(7, 'Cancha mixta de futbol y basqu', '17.081740,17.081474,17.081473,17.081739,17.081740', '-96.744807,-96.744804,-96.744624,-96.744615,-96.744807', 'Cancha mixta de futbol y basquetbol', '', 'https://www.google.com/maps/embed?pb=!4v1540939379142!6m8!1m7!1sCAoSLEFGMVFpcE1FUVZqTWFDLXRiWUZNQU1Ubl9NZlk5MDJ6bUlFQWg3ZDR5THE2!2m2!1d17.08159771571005!2d-96.74473313070405!3f357.9422961217963!4f-8.597128591721301!5f0.7820865974627469'),
(8, 'Cancha de Pelota?Azteca', '17.080694,17.079750,17.079760,17.080684,17.080694', '-96.745651,-96.745615,-96.745401,-96.745444,-96.745651', 'Cancha de Pelota Azteca', '', 'https://www.google.com/maps/embed?pb=!4v1540940455589!6m8!1m7!1sCAoSLEFGMVFpcE1Xcl90aXN6d05UWGctS3NzbFk4WjVULTE4bEtnZmpSdFlQUm9o!2m2!1d17.08049583249711!2d-96.74539760637889!3f244.5730669380461!4f-4.628625146986522!5f0.7820865974627469');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratorios`
--

CREATE TABLE `laboratorios` (
  `id` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `latitudes` text COLLATE utf8_spanish_ci NOT NULL,
  `longitudes` text COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `imagenPlana` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `imagen3d` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `laboratorios`
--

INSERT INTO `laboratorios` (`id`, `nombre`, `latitudes`, `longitudes`, `descripcion`, `imagenPlana`, `imagen3d`) VALUES
('I', 'Laboratorio de Ing. Electrica', '17.078885,17.079085,17.079167, 17.078965,17.078885', ' -96.744884, -96.744807,-96.745062,-96.745132, -96.744884', 'Laboratorio de eléctrica  ', 'recursos/laboratorios/lab_electrica.jpg', 'https://www.google.com/maps/embed?pb=!4v1540943141975!6m8!1m7!1sCAoSLEFGMVFpcFBQaXFRN21jR0FPa2lHejYwdHlwUEZKODh1Z0dnQlBjS1YwazFY!2m2!1d17.07894952639822!2d-96.74478907511106!3f320.3909281396822!4f-3.2640306146524836!5f0.7820865974627469'),
('II', 'Laboratorio de Ing. Industrial', '17.079161,17.079286, 17.079325,17.079198,17.079161', '-96.744765,-96.744719,-96.744839,-96.744885,-96.744765', 'Laboratorio de Simulación de Ing. Industrial', 'recursos/laboratorios/lab_simulacion_industrial.jpg', ''),
('III', 'Laboratorio de Ing. Quimica', '17.078495, 17.078643,17.078722, 17.078571 ,17.078495', '-96.744557,-96.744505,-96.744729,-96.744784,-96.744557', 'Laboratorio de quimica', 'recursos/laboratorios/lab_quimica.jpg', 'https://www.google.com/maps/embed?pb=!4v1540942202821!6m8!1m7!1sCAoSLEFGMVFpcE5EYkpmOUhOY3hBTFRUSFU5UDFiekh4Tnd1bkNOY1hGMDRiRGN4!2m2!1d17.07857188343819!2d-96.74444213889764!3f277.00492658462247!4f7.28085550259172!5f0.7820865974627469'),
('IV', 'Laboratorio de Ing. Mecánica', '17.078643,17.078796,17.078870,17.078722,17.078643', ' -96.744505,-96.744451, -96.744676,-96.744729,-96.744505', 'Laboratorio de Mecanica y departamento de Ing.Mecanica', 'recursos/laboratorios/lab_mecanica.jpg', 'https://www.google.com/maps/embed?pb=!4v1540942360590!6m8!1m7!1sCAoSLEFGMVFpcE41MGpBR1ZJSDJXV0pGWkUtSTVOdTlid1p6S2VrY01ycjJXd2Zq!2m2!1d17.07870616046591!2d-96.74441290094964!3f304.8941467626413!4f-4.715734082928691!5f0.7820865974627469'),
('IX', 'Laboratorio de Ing. Electronic', '17.078846,17.078617, 17.078554,17.078785,17.078846', ' -96.743833,-96.743910, -96.743707,-96.743630,-96.743833', 'Laboratorio de Ing. Electronica', 'recursos/laboratorios/lab_electronica.jpg', 'https://www.google.com/maps/embed?pb=!4v1540941043498!6m8!1m7!1sCAoSLEFGMVFpcE1lUXRjMzl5QW1FSF8wZnA4d0RwcngwelBhcmVjdS1ac0R2blZz!2m2!1d17.07852387234952!2d-96.7438117292011!3f6.9906777034264564!4f3.6095480647235547!5f0.7820865974627469'),
('V', 'Laboratorio de Ing. Quimica', '17.078282,17.078320,17.077936,17.077899,17.078282', ' -96.744327,-96.744443,-96.744579,-96.744463,-96.744327', 'Laboratorio de quimica (ciantitativa,cualitativa, analisis clinico y fisico-quimica)', 'recursos/laboratorios/lab_quim_analisis_clinico.jpg', ''),
('VI', 'Laboratorio de Ing. Civil', '17.078523,17.078737,17.078824,17.078608,17.078523', ' -96.744081,-96.744005, -96.744273,-96.744349, -96.744081', 'Laboratorio de Ing.Civil', 'recursos/laboratorios/lab_civil.jpg', 'https://www.google.com/maps/embed?pb=!4v1540942487541!6m8!1m7!1sCAoSLEFGMVFpcE41MGpBR1ZJSDJXV0pGWkUtSTVOdTlid1p6S2VrY01ycjJXd2Zq!2m2!1d17.07870616046591!2d-96.74441290094964!3f96.39113457850557!4f-0.2622995946024247!5f0.7820865974627469'),
('VII', 'Laboratorio Computo', '17.079268,17.079022,17.078987,17.079235,17.079268', ' -96.744418,-96.744503,-96.744393,-96.744311,-96.744418', 'Laboratorio de computo', 'recursos/laboratorios/centro_computo.jpg', 'https://www.google.com/maps/embed?pb=!4v1540942576714!6m8!1m7!1sCAoSLEFGMVFpcFBsRlBldjktRjFkbjRYUlJyWWpudE1Bbld1TTJNS2xjZjhRMHh0!2m2!1d17.07892858673662!2d-96.74435320004102!3f60.945545134641755!4f-0.27093215184845576!5f0.7820865974627469'),
('VIII', 'Laboratorio de Ing. Quimica', '17.079287, 17.079484,17.079567, 17.079370,17.079287', '-96.743818, -96.743748,-96.744014,-96.744082,-96.743818', 'Laboratorio de quimica (control ambiental, microbiologia)', '', ''),
('X', 'Laboratorio de Ing. Industrial', '17.076932,17.076738,17.076625,17.076821,17.076932', '-96.743606, -96.743675,-96.743327, -96.743258,-96.743606', 'Laboratorio de Ing. Industrial', 'recursos/laboratorios/lab_industrial.jpg', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrativos`
--
ALTER TABLE `administrativos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `aulas`
--
ALTER TABLE `aulas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `deportivo`
--
ALTER TABLE `deportivo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `laboratorios`
--
ALTER TABLE `laboratorios`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
