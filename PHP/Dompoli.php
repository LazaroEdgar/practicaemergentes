<?php
header("access-control-allow-origin: *");
require("phpsqlajax_dbinfo.php");
// Start XML file, create parent node
$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);
//base de datos
$connection=mysqli_connect ('localhost', $username, $password);
if (!$connection) {
  die('Not connected : ' . mysqli_error());
}
//esquema de base de datos
$db_selected = mysqli_select_db($connection,$database);
if (!$db_selected) {
  die ('Can\'t use db : ' . mysqli_error());
}
header("Content-type: text/xml");
// Select all table
$tabla = array("aulas","administrativos","laboratorios","deportivo");
foreach($tabla as $i => $value){
  $query = "SELECT * FROM ".$tabla[$i];
  $result = mysqli_query($connection,$query);
  if (!$result) {
    die('Invalid query: ' . mysqli_error());
  }
  ///xml
  while ($row=mysqli_fetch_assoc($result)){
    // Add to XML document node
    $node = $dom->createElement("marker");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("latitudes",utf8_encode($row['latitudes']));
    $newnode->setAttribute("longitudes",utf8_encode($row['longitudes']));
  	$newnode->setAttribute("nombre",utf8_encode($row['nombre']));
    $newnode->setAttribute("descripcion",utf8_encode($row['descripcion']));
    $newnode->setAttribute("tipo",utf8_encode($tabla[$i]));
    $newnode->setAttribute("img",utf8_encode($row['imagenPlana']));
    $newnode->setAttribute("img360",utf8_encode($row['imagen3d']));
  }
}
echo $dom->saveXML();
?>
